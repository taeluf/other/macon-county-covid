# Cdc vaccinations
This is just an info sheet about the cdc vaccinatios data

Source: https://data.cdc.gov/Vaccinations/COVID-19-Vaccinations-in-the-United-States-County/8xkx-amqh
Latest Data just for Macon County: https://data.cdc.gov/resource/8xkx-amqh.csv?fips=17115

## The Columns I care about
You may care about other columns, but I'm not using them right now.

All of the rows in my copy of the data SHOULD be macon county, IL. (unless there is an error)
- date
- Series_Complete_Yes: Number of people fully vaccinated in the county
- Series_Complete_Pop_Pct: Percentage of total macon county population that is fully vaccinated
- Administered_Dose1_Recip: Number of people with at least one vaccine dose in the county
    - The description below says by "State of Residence", BUT for Sept 10, 2021, the data shows 42,212 people fully vaccinated (Series_Complete_Yes), and 54,027 people who've at least had their first dose (Administered_Dose1_Recip). This means about 12,000 people have a first does but are not (yet) fully vaccinated. I'm extremely confident this is only for Macon County Residents. It is also inline with what the CDC displays on their website specific to the county in the count tracker view
- Administered_Dose1_Pop_Pct: Percentage of total macon county population that has at least one covid vaccine dose.
    - See my notes on Administered_Dose1_Recip

## The Columns
These descriptions are programmatically copied from the cdc website.

- Date: Date data are reported on CDC COVID Data Tracker
- FIPS: Federal Information Processing Standard State Code
- MMWR_week: MMWR Week
- Recip_County: County of residence
- Recip_State: Recipient State
- Series_Complete_Pop_Pct: Percent of people who are fully vaccinated (have second dose of a two-dose vaccine or one dose of a single-dose vaccine) based on the jurisdiction and county where recipient lives
- Series_Complete_Yes: Total number of people who are fully vaccinated (have second dose of a two-dose vaccine or one dose of a single-dose vaccine) based on the jurisdiction and county where recipient lives
- Series_Complete_12Plus: Total number of people 12+ who are fully vaccinated (have second dose of a two-dose vaccine or one dose of a single-dose vaccine) based on the jurisdiction where recipient lives
- Series_Complete_12PlusPop_Pct: Percent of people 12+ who are fully vaccinated (have second dose of a two-dose vaccine or one dose of a single-dose vaccine) based on the jurisdiction where recipient lives
- Series_Complete_18Plus: Total number of people 18+ who are fully vaccinated (have second dose of a two-dose vaccine or one dose of a single-dose vaccine) based on the jurisdiction and county where recipient lives
- Series_Complete_18PlusPop_Pct: Percent of people 18+ who are fully vaccinated (have second dose of a two-dose vaccine or one dose of a single-dose vaccine) based on the jurisdiction and county where recipient lives
- Series_Complete_65Plus: Total number of people 65+ who are fully vaccinated (have second dose of a two-dose vaccine or one dose of a single-dose vaccine) based on the jurisdiction where recipient lives
- Series_Complete_65PlusPop_Pct: Percent of people 65+ who are fully vaccinated (have second dose of a two-dose vaccine or one dose of a single-dose vaccine) based on the jurisdiction where recipient lives
- Completeness_pct: Represents the proportion of fully vaccinated people whose Federal Information Processing Standards (FIPS) code is reported and matches a valid county FIPS code in the jurisdiction.  
- Administered_Dose1_Recip: People with at least one Dose by State of Residence
- Administered_Dose1_Pop_Pct: Percent of Total Pop with at least one Dose by State of Residence
- Administered_Dose1_Recip_12Plus: People 12+ with at least one Dose by State of Residence
- Administered_Dose1_Recip_12PlusPop_Pct: Percent of 12+ Pop with at least one Dose by State of Residence
- Administered_Dose1_Recip_18Plus: People 18+ with at least one Dose by State of Residence
- Administered_Dose1_Recip_18PlusPop_Pct: Percent of 18+ Pop with at least one Dose by State of Residence
- Administered_Dose1_Recip_65Plus: People 65+ with at least one Dose by State of Residence
- Administered_Dose1_Recip_65PlusPop_Pct: Percent of 65+ Pop with at least one Dose by State of Residence
- SVI_CTGY: SVI
- Series_Complete_Pop_Pct_SVI: Percent of Population Fully Vaccinated/SVI
- Series_Complete_12PlusPop_Pct_SVI: Percent of 12+ Pop Fully Vaccinated/SVI
- Series_Complete_18PlusPop_Pct_SVI: Percent of 18+ Pop Fully Vaccinated/SVI
- Series_Complete_65PlusPop_Pct_SVI: Percent of 65+ Pop Fully Vaccinated/SVI

## Generating column names
src: https://data.cdc.gov/Vaccinations/COVID-19-Vaccinations-in-the-United-States-County/8xkx-amqh

Extracting Column Names: I ran ths in the browser console
```javascript
columns = document.querySelectorAll('tr.column-summary');
rows = [];
for (c of columns){
    name=c.querySelector('td.column-name').innerText;
    description = c.querySelector('td.column-description').innerText;
    rows.push({name:name, description:description});
}
console.log(JSON.Stringify(rows));
```

Convert the column names to nicer format for md:
```php

```
