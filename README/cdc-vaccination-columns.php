<?php

$cols = json_decode(file_get_contents(substr(__FILE__,0,-4).'.json'), true);

$str = '';
foreach ($cols as $c){
    $name=$c['name'];
    $description = $c['description'];
    $str .="\n- $name: $description";
}

echo "\n\n";
echo $str."\n\n";
