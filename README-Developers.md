# Development Info
This document is old & was written for version `v0.1`. It's probably all still helpful, maybe? I don't know. I don't care. If you care, send a pull request or open an issue & then I'll care.

### v0.2 vs v0.1
v0.1 had the data processing built into it. v0.2 has had the .ods to spreadsheet software extracted into it's own library & is now depending upon that library.

### Disclaimer
The code to do all this is fresh. There are no automated tests. The `.ods` files are my true source of data. Everything else is generated. There's a possibility of errors, though I think it's pretty unlikely there's any major issues.

## Overview
- `bin/` contains files used to convert `.ods` files into `.csv`, `.json`, and `.sql` files, and generate a `.sqlite` database file with all tables created & rows inserted.
    - `bin/build.php` is the general process, step-by-step & should be executed via cli
    - `bin/src.php` contains functions to do the actual complicated stuff.
- `data/` contains `.ods` files
    - The source data for all other data dirs in this repository
    - I use [`libreoffice`](https://www.libreoffice.org/), a free & open-source alternative to Microsoft Office.
- `data-*/` directories are generated as follows:
    1. `data/source.ods` is converted to `data-csv/source.csv` with `libreoffice` cli
    2. `data-csv/source.csv` is converted to json using [`league/csv`](https://csv.thephpleague.com/)
    3. `data-json/source.json` is converted to sql using [`taeluf/json-to-mysql`](https://tluf.me/json-to-mysql)
    4. `data.sqlite` is created by an internal script, which scans `data-sql/` and executes all the sql generated in the previous step.
- `data/subdir/*` contains original data sources. like `data/mchd/` contains images of MCHD's weekly releases. (I'll probably add more later) 


## Install
This uses `php` and has several dependencies which are installed via `composer`.  

**CONTRIBUTE:** Write nicer install instructions here

You may be able to skip steps 1-3 if you already have php setup.
1. [Install `git`](https://git-scm.com/downloads)
2. Install Language Dependencies (php, pdo, sqlite) (see below)
3. [Install Composer](https://getcomposer.org/doc/00-intro.md)
4. `git clone` this repository
5. `cd` into the downloaded repo
6. run `composer install`

OR, you can include this as a dependency in YOUR composer.json. It's `taeluf/data.macon-county-covid` version `v0.1.x-dev`

### Language Dependencies
This package uses `php`. You will need to install:
- [`php`](https://www.php.net/manual/en/install.php) 
- [`php's PDO driver`](https://www.php.net/manual/en/book.pdo.php)
- [`php's sqlite driver`](https://www.php.net/manual/en/book.sqlite3.php)

I think `PDO` and `sqlite` are automatically installed with php on most systems.

You **do not need** a server. This is all run via cli.

## Usage
1. Update data in `data/*.ods`
2. Execute `bin/build.php` (if it fails to execute, try to `chmog ug+x bin/build.php`)
3. Use the generated sqlite databases, csv, or json files as you wish.
