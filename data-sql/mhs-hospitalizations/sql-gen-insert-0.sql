    INSERT INTO `mhs_hospitalizations` 
        ( `date`,`time`,`hospitalized`,`novax_hosp`,`vax_hosp`,`icu`,`novax_icu`,`vax_icu`,`ventilated`,`novax_vent`,`vax_vent`,`novax_hosp_percent`,`novax_icu_percent`,`novax_vent_percent` )
    VALUES
( '2021-08-20', '09:00:00 AM', '80', '69', '11', '27', '23', '4', '19', '18', '1', 86.25, 85.19, 94.74),
( '2021-08-27', '01:00:00 PM', '73', '59', '14', '26', '22', '4', '20', '18', '2', 80.82, 84.62, 90),
( '2021-09-03', '11:00:00 AM', '79', '65', '14', '28', '27', '1', '16', '16', '0', 82.28, 96.43, 100),
( '2021-09-10', '10:00:00 AM', '79', '61', '18', '26', '22', '4', '16', '15', '1', 77.22, 84.62, 93.75),
( '2021-09-17', '10:00:00 AM', '76', '52', '24', '17', '14', '3', '7', '6', '1', 68.42, 82.35, 85.71),
( '2021-09-24', '09:00:00 AM', '67', '49', '18', '17', '16', '1', '9', '9', '0', 73.13, 94.12, 100),
( '2021-10-01', '10:00:00 AM', '55', '42', '13', '19', '16', '3', '11', '9', '2', 76.36, 84.21, 81.82),
( '2021-10-08', '10:00:00 AM', '47', '38', '9', '19', '18', '1', '9', '8', '1', 80.85, 94.74, 88.89)
;
