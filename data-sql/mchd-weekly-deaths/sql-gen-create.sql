    DROP TABLE IF EXISTS `mchd_weekly_deaths`;

    CREATE TABLE `mchd_weekly_deaths`
    (
    `date` DATE, `total_deaths` int(10), `url` VARCHAR(178), `male` int(10), `female` int(10), `black` int(10), `white` int(10), `other_race` int(10), `unknown_race` int(10), `age_30_to_39` int(10), `age_40_to_49` int(10), `age_50_to_59` int(10), `age_60_to_69` int(10), `age_70_to_79` int(10), `age_80_to_89` int(10), `age_90_to_99` int(10), `age_100_plus` int(10)
    )
    ;
    