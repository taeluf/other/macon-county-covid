    INSERT INTO `cdc_7_days_rolling` 
        ( `date`,`cases`,`cases_per_100k`,`test_positivity`,`new_hospital_admissions`,`percent_icu_beds_used`,`percent_hospital_beds_used` )
    VALUES
( '2021-08-21', '494', '474.96', '8.69', '24', '36.36', NULL),
( '2021-08-22', '494', '474.96', '8.4', '26', '33.77', NULL),
( '2021-08-23', '480', '461.5', '8.48', '27', '32.04', '9.11'),
( '2021-08-24', '375', '360.55', NULL, '26', '31.17', '8.91'),
( '2021-08-25', '416', '399.97', NULL, NULL, NULL, NULL)
;
