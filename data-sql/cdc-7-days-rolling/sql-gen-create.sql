    DROP TABLE IF EXISTS `cdc_7_days_rolling`;

    CREATE TABLE `cdc_7_days_rolling`
    (
    `date` DATE, `cases` int(10), `cases_per_100k` FLOAT, `test_positivity` FLOAT, `new_hospital_admissions` int(10), `percent_icu_beds_used` FLOAT, `percent_hospital_beds_used` FLOAT
    )
    ;
    