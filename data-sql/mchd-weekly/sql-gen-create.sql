    DROP TABLE IF EXISTS `mchd_weekly`;

    CREATE TABLE `mchd_weekly`
    (
    `date` DATE, `cases` int(10), `released_from_isolation` int(10), `in_home_isolation` int(10), `hospitalized` int(10), `deaths` int(10)
    )
    ;
    