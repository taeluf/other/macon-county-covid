    DROP TABLE IF EXISTS `cdc_vaccinations`;

    CREATE TABLE `cdc_vaccinations`
    (
    `date` DATE, `fips` int(10), `mmwr_week` int(10), `recip_county` VARCHAR(62), `recip_state` VARCHAR(52), `series_complete_pop_pct` FLOAT, `series_complete_yes` int(10), `series_complete_12plus` int(10), `series_complete_12pluspop` FLOAT, `series_complete_18plus` int(10), `series_complete_18pluspop` FLOAT, `series_complete_65plus` int(10), `series_complete_65pluspop` FLOAT, `completeness_pct` FLOAT, `administered_dose1_recip` int(10), `administered_dose1_pop_pct` FLOAT, `administered_dose1_recip_12plus` int(10), `administered_dose1_recip_12pluspop_pct` FLOAT, `administered_dose1_recip_18plus` int(10), `administered_dose1_recip_18pluspop_pct` FLOAT, `administered_dose1_recip_65plus` int(10), `administered_dose1_recip_65pluspop_pct` FLOAT, `svi_ctgy` VARCHAR(51), `series_complete_pop_pct_svi` int(10), `series_complete_12pluspop_pct_svi` int(10), `series_complete_18pluspop_pct_svi` int(10), `series_complete_65pluspop_pct_svi` int(10), `metro_status` VARCHAR(55), `series_complete_pop_pct_ur_equity` int(10), `series_complete_12pluspop_pct_ur_equity` int(10), `series_complete_18pluspop_pct_ur_equity` int(10), `series_complete_65pluspop_pct_ur_equity` int(10)
    )
    ;
    