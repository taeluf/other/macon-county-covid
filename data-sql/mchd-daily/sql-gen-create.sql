    DROP TABLE IF EXISTS `mchd_daily`;

    CREATE TABLE `mchd_daily`
    (
    `date` DATE, `cases` int(10), `released_from_isolation` int(10), `in_home_isolation` int(10), `hospitalized` int(10), `deaths` int(10)
    )
    ;
    