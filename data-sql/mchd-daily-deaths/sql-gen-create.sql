    DROP TABLE IF EXISTS `mchd_daily_deaths`;

    CREATE TABLE `mchd_daily_deaths`
    (
    `report_date` VARCHAR(58), `sex` VARCHAR(51), `age_range` VARCHAR(54), `url` VARCHAR(174)
    )
    ;
    