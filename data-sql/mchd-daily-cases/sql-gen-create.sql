    DROP TABLE IF EXISTS `mchd_daily_cases`;

    CREATE TABLE `mchd_daily_cases`
    (
    `report_date` VARCHAR(58), `total_confirmed_cases` int(10), `currently_hospitalized` int(10), `total_deaths` int(10), `url` VARCHAR(174)
    )
    ;
    