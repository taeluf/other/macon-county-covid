#!/usr/bin/env php  
<?php  
  
$root_dir = dirname(__DIR__);  
require($root_dir.'/vendor/autoload.php');  
  
  
// .ods files are NEVER overwritten (except in case of error, maybe? Do use version control)  
\Tlf\build_csv_conversions(  
      
    // you could do this:  
        // json_decode(file_get_contents(__DIR__.'/settings.json'));  
    // or a for loop over a scandir() or an array of settings files & run many separate conversions  
    [   
        // all settings shown are the defaults  
          
        'source_dir' => $root_dir.'/data/',  
  
        'path_prefix'=>$root_dir.'/data-',  
  
        // bugfix: Fileformat encoding leaves some characters incompatible with utf8, so apply a fix with libreoffice cli  
        'use_filetype_conversion'=>true,  
  
        // WARNING: OVERWRITES FILES if true  
        // none of the files should be edited, generally. They're meant to be built from .ods spreadsheets  
        // Good to set them false if testing some kind of error  
        //  
        'csv'=>true,  
        'json'=>true,  
        'sql'=>true,  
  
        'sqlite'=>true,  
  
    ]  
);  
