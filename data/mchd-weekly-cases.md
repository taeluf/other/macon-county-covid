# Weekly COVID Case Counts | Macon County Department of Health
This data counts the number of cases found in Macon County, along with demographic data.

## Data Source
This data comes from Macon County Health Department. I look at their daily case-number infographics on facebook & write the numbers into a spreadsheet.

## Links
- [MCHD Facebook](https://www.facebook.com/Macon-County-Health-Department-Decatur-IL-116643335063685/)

## Notes
- The infographics are released on Fridays about 5pm by MCHD
- Demographics data includes 2 of the biological sexes, age, and race. Intersectionality data (such as "asian women ages 30-39") is NOT available 
    - On Oct 12, 2021 I noticed their was also an "unknown" metric for "gender", but I have not added this to my spreadsheets.
- The demographics data is for cases, not deaths.

## General MCHD Data Reporting Notes
- I have not collected case numbers throughout the pandemic, so I only have limited data available. 
- During periods of the pandemic, MCHD stopped posting upates
- MCHD also used to post the infographics to their website. They stopped in August 2021.
- MCHD does NOT make data available in spreadsheet format.
- I have made FOIA requests for COVID data in a raw format, and my request was denied. They claimed it was unduly burdensome. I've heard from one other person who had a similar experience.
