## Facebook post my MCHD for Oct 22, 2021 weekly report

Link: https://www.facebook.com/permalink.php?story_fbid=4480610985333543&id=116643335063685

Since the Macon County Health Department’s last update on Thursday, October 21, 2021, there have been 31 new COVID-19 cases reported in Macon County. Although there have been 31 new cases, one previously-reported case was determined to be from another county and was transferred appropriately. Thus, the total number of COVID-19 cases is 15,457 since the start of the pandemic. The total number of hospitalizations is 12.
The Illinois Department of Public Health (IDPH) and the Macon County Health Department (MCHD) have identified 453 variant COVID-19 cases in Macon County. The numbers of cases are as follows:
• 209 cases of B.1.617.2/ AY.2/AY.12/AY.25/AY.3/AY.3.1/AY.4/AY.5/AY.6 (Delta variants)
• 143 cases of B.1.1.7 (Alpha variant)
• 87 cases of P.1/P.1.1 (Gamma variants)
• 7 cases of B.1.427/B.1.429 (Epsilon variants)
• 4 cases of B.1.351 (Beta variant)
• 3 cases of B.1.621 (Mu variant)
This data above represents information received from IDPH since April 1, 2021, when it was confirmed Macon County had two cases of variant strains of COVID-19. At this time, IDPH does not sequence every COVID-19 test for variants; therefore, the number of variant cases is likely higher. According to the Centers for Disease Control and Prevention, variants of concern are those in “which there is evidence of an increase in transmissibility, more severe disease (increased hospitalizations or deaths), significant reduction in neutralization by antibodies generated during previous infection or vaccination, reduced effectiveness of treatments or vaccines, or diagnostic detection failures.
