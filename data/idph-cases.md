# COVID Cases in Macon County | Illinois Department of Public Health
This data has daily counts for covid cases, deaths, and tests.

## Data Source
This data comes from the Illinois Department of Public Health cases, tests, and deaths by day data.

### Links
- [IDPH Data Portal](https://dph.illinois.gov/covid19/data/data-portal.html) which has other IDPH data downloads as well
- [IDPH County Cases by day](https://dph.illinois.gov/covid19/data/data-portal/cases-tests-and-deaths-day1.html) is the actual source page for this data.
    - the CSV download works again
- [Direct .json download](https://idph.illinois.gov/DPHPublicInformation/api/COVID/GetCountyHistorical?countyName=Macon)

## Notes about the data
- Sometimes the IDPH case data has errors in the most recent rows. On Mondays & Tuesdays, sometimes Saturday-Monday are all wrong. It always gets corrected the next day. On other days, it's usually only one day of data that's wrong. I don't add the erroneous rows to the compiled data.
    - They added a few columns, ones that I was already calculating, so I'm not capturing the new columns
- In Early october / late september, IDPH changed the format of their data. The CSV file download stopped working. Then early october, they also changed the website URLs, so the old links no longer work.

## Other Notes
- I use [convertcsv.com](https://www.convertcsv.com/json-to-csv.htm) to convert the `.json` data to `.csv` for spreadsheet programs when the csv download isn't working
- OLD, broken link:
    - [idph county data downloads](https://dph.illinois.gov/content/covid-19-county-cases-tests-and-deaths-day) - link is broken. I don't know if this page is archived on archive.org

