# Variant COVID Cases | Macon County Department of Health
This data counts the covid variants found in Macon County

## Data Source
This data comes from Macon County Health Department. I look at their daily case-number infographics on facebook & write the numbers into a spreadsheet.

### Links
- [MCHD Facebook](https://www.facebook.com/Macon-County-Health-Department-Decatur-IL-116643335063685/)

## Notes about this data set
- Variant reports are shared in MCHD's Facebook COVID updates (in the text of the post). Only a few of the covid updates include variant updates.
- Not all positive cases are sequenced for variants. It seems a very small portion are sequenced, actually.

## General MCHD Data Reporting Notes
- I have not collected case numbers throughout the pandemic, so I only have limited data available. 
- During periods of the pandemic, MCHD stopped posting upates
- MCHD also used to post the infographics to their website. They stopped in August 2021.
- MCHD does NOT make data available in spreadsheet format.
- I have made FOIA requests for COVID data in a raw format, and my request was denied. They claimed it was unduly burdensome. I've heard from one other person who had a similar experience.
